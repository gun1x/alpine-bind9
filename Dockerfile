FROM alpine:latest
RUN apk add --no-cache bind
EXPOSE 53
CMD ["named", "-c", "/etc/bind/named.conf", "-4", "-g", "-u", "named"]
